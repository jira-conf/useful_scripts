#!/usr/bin/bash

#This script clear work log of passed issues and their subtasks

RED='\033[0;31m'
CYAN='\033[1;36m'
NC='\033[0m'
if [ "$#" == "0" ];then
    echo -e "${RED} Not enoght parameters!${NC}\n"\
    "(All parameters must be send before list of issues!)\n"\
    "List of params:\n"\
    "${CYAN}--sub${NC} - use this parameter to clear work log in subtasks of issues that will be passed next.\n"\
    "E.g. ${0} [${CYAN}--sub${NC}] (ISSUE_KEY|ISSUE_ID)\n"
    exit 1
fi

if [ "$1" == "--sub" ];then
    echo ${SUB}
    SUB=1
    FLAG='--sub'
fi


# JIRA_API_AUTH - encode your jira user's data in base64 
# format before encoding must be next: jira_username:jira_user_password
# also user must have enought privileges to remove worklog data
# JIRA_URL - jira server address | F.g. http://192.168.20.20:8080
# Create next variables one of two ways:
# 1) uncomment and set two variables below;
# 2) set variables below as enivronment variables.

#JIRA_API_AUTH=
#JIRA_URL=


for arg in "$@"
do
    if [ $arg != $0 ] && [ "${arg}" != "--sub" ];then
	echo $arg
	######################### start_of_subtask_processing ########################
	# worklog of subtasks of issue also will be cleaned.
	echo 'SUB: '$SUB
	if [ $SUB -eq 1 ];then
	    subtasks=$(curl --noproxy '*' --location --request GET "${JIRA_URL}/rest/api/2/issue/${arg}" \
	    --header "Authorization: Basic ${JIRA_API_AUTH}" \
	    | jq '.fields | .subtasks | .[] | .id')
	    SUBTASK_IDS=$(echo $subtasks | tr -d \")
	    IFS=' ' read -r -a subtask_ids <<< "$SUBTASK_IDS"
	    echo 'SUBTASKS: '${subtask_ids[@]}

	    for subtask in ${subtask_ids[@]}
	    do
		SUBTASKS_WORKLOG_IDS=$(curl --noproxy '*' --location --request GET "${JIRA_URL}/rest/api/2/issue/${subtask}/worklog" \
		--header "Authorization: Basic ${JIRA_API_AUTH}" | jq ".worklogs | .[] | .id" )
		SUBTASKS_WORKLOG_IDS=$(echo $SUBTASKS_WORKLOG_IDS | tr -d \")
		IFS=' ' read -r -a subtasks_worklog_ids <<< "$SUBTASKS_WORKLOG_IDS"
		
		for subtask_worklog in ${subtasks_worklog_ids[@]}
		do
		    curl --noproxy '*' --location --request DELETE "${JIRA_URL}/rest/api/2/issue/${subtask}/worklog/${subtask_worklog}?adjustEstimate=new&newEstimate=0" \
		    --header "Authorization: Basic ${JIRA_API_AUTH}"
		done
	    done
	fi
	############################ end_of_subtasks_processing ########################

	JSON_WORKLOG_IDS=$(curl --noproxy '*' --location --request GET "${JIRA_URL}/rest/api/2/issue/${arg}/worklog" \
	--header "Authorization: Basic ${JIRA_API_AUTH}" | jq ".worklogs | .[] | .id" )
	JSON_WORKLOG_IDS=$(echo $JSON_WORKLOG_IDS | tr -d \")
	IFS=' ' read -r -a worklog_ids <<< "$JSON_WORKLOG_IDS"
	echo ${worklog_ids[@]}
	
	for worklog in ${worklog_ids[@]}
	do
	    curl --noproxy '*' --location --request DELETE "${JIRA_URL}/rest/api/2/issue/${arg}/worklog/${worklog}?adjustEstimate=new&newEstimate=0" \
	    --header "Authorization: Basic ${JIRA_API_AUTH}"
	done
	
	curl --noproxy '*' --location --request PUT "${JIRA_URL}/rest/api/2/issue/${arg}" \
	--header "Authorization: Basic ${JIRA_API_AUTH}" \
	--header 'Content-Type: application/json' \
	--data-raw '{"update":{"timetracking": [{"edit": {"originalEstimate": "0"}}]}}'
    fi
done
